package uz.pdp.appstudentcrud.service;

import uz.pdp.appstudentcrud.entity.Group;
import uz.pdp.appstudentcrud.entity.Student;
import uz.pdp.appstudentcrud.payload.StudentDTO;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService{

    private static StudentServiceImpl instance;
    public List<Student> students = Collections.synchronizedList(new ArrayList<>());
    private static Lock lock = new ReentrantLock();
    private static Logger logger = Logger.getLogger("Studentservice");
    private StudentServiceImpl() {
    }
    public static StudentServiceImpl getInstance() {

        if (Objects.isNull(instance)) {
            lock.lock();
            if (Objects.isNull(instance)) {
                instance = new StudentServiceImpl();
            }
            lock.unlock();
        }

        return instance;
    }
    @Override
    public List<StudentDTO> getByGroup(Integer groupId) {

        logger.log(Level.INFO,"get students by group param ->"+groupId);

        List<StudentDTO> studentDTOList = students
                .stream()
                .filter(student -> student.getGroup().getid().equals(groupId)||student.getAddress().getIdNumber().equals(groupId))
                .map(this::toDTO)
                .collect(Collectors.toList());
        return studentDTOList;
    }

    @Override
    public StudentDTO getById(Integer id) {
        Student student = getStudentByIdOrElseThrow(id);
        return toDTO(student);
            }

    @Override
    public StudentDTO add(StudentDTO studentDTO) {

        int id = students.size() + 1;

        List<Group> groups = GroupServiceImpl.getInstance().groups;

        GroupService groupService = GroupServiceImpl.getInstance();
        Group group= groupService.getByIdOrElseThrow(studentDTO.getGroupid());


        Student student = new Student(
                id,
                studentDTO.getFirstname(),
                studentDTO.getLastname(),
                studentDTO.getPhoheNumer(),
                studentDTO.getBioFilePath(),
                studentDTO.getBirthDate(),
                 group
                                                );
        students.add(student);
        return toDTO(student);

    }

    @Override
    public StudentDTO edit(Integer id, StudentDTO studentDTO) {

        Student student = getStudentByIdOrElseThrow(id);
student.setLastname(studentDTO.getLastname());
student.setFirstname(studentDTO.getFirstname());
student.setBirthDate(studentDTO.getBirthDate());
student.setBioFilePath(studentDTO.getBioFilePath());
student.setPhoheNumer(studentDTO.getPhoheNumer());


        GroupService groupService = GroupServiceImpl.getInstance();
        Group group = groupService.getByIdOrElseThrow(studentDTO.getGroupid());
        student.setGroup(group);
        StudentDTO studentDTO1 = toDTO(student);
        return studentDTO1;

    }

    @Override
    public boolean delete(Integer id) {
        try {

            Student student = getStudentByIdOrElseThrow(id);

            students.remove(student);

            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String read(Integer id) {
        Student student = getStudentByIdOrElseThrow(id);

        Path path = Paths.get(student.getBioFilePath());
        return readFileViaFileInputStream(path);
    }

    @Override
    public boolean serialize() {
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("db/countries.txt"))) {

            objectOutputStream.writeObject(students);
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deserialize() {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("db/countries.txt"))){

            students = (List<Student>)objectInputStream.readObject();

            return true;
        }catch (IOException e){
            e.printStackTrace();
            return false;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }



    public StudentDTO toDTO(Student student) {
        return new StudentDTO(

                student.getId(),
                student.getFirstname(),
                student.getLastname(),
                student.getPhoheNumer(),
                student.getBirthDate(),
                student.getBioFilePath(),
                student.getGroup().getid(),
                student.getAddress().getIdNumber()



        );
    }

    private Student getStudentByIdOrElseThrow(Integer id) {
        Optional<Student> optionalStudent = students
                .stream()
                .filter(student -> student.getId().equals(id))
                .findFirst();

        Student student = optionalStudent.orElseThrow(() -> new RuntimeException("Student not found with id : " + id));
        return student;
    }

    private String readFileViaFileReader(Path path) {
        try(Reader reader = new FileReader(path.toFile())) {
            StringBuilder stringBuilder = new StringBuilder();
            int symbolNumber = reader.read();
            while (symbolNumber != -1){
                stringBuilder.append((char) symbolNumber);
                symbolNumber = reader.read();
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String readFileViaFileInputStream(Path path) {
        try(InputStream inputStream = new FileInputStream(path.toFile())) {
            byte[] bytes = inputStream.readAllBytes();
            return new String(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
