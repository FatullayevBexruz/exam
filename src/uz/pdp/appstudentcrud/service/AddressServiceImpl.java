package uz.pdp.appstudentcrud.service;

import uz.pdp.appstudentcrud.entity.Address;
import uz.pdp.appstudentcrud.entity.Group;
import uz.pdp.appstudentcrud.payload.AddressDTO;
import uz.pdp.appstudentcrud.payload.GroupDTO;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class AddressServiceImpl implements AddressService{

    private static AddressServiceImpl instance;

    public List<Address> addresses = Collections.synchronizedList(new ArrayList<>());

    private static Lock lock = new ReentrantLock();

    private AddressServiceImpl() {}

    public static AddressServiceImpl getInstance() {

        if (Objects.isNull(instance)) {
            lock.lock();
            if (Objects.isNull(instance))
                instance = new AddressServiceImpl();
            lock.unlock();
        }
        return instance;
    }
    @Override
    public List<AddressDTO> all() {
        return addresses
                .stream()
                .map(address -> new AddressDTO((Integer) address.getIdNumber(),address.getRegion(),address.getCity(),address.getAddressLine()))
                .collect(Collectors.toList());
    }

    @Override
    public AddressDTO add(AddressDTO addressDTO) {

        int id = addresses.size() + 1;
        Address address = new Address(
                id,
                addressDTO.getRegion(),
                addressDTO.getCity(),
                addressDTO.getAddressLine()
        );
        addresses.add(address);
        addressDTO.setIdNumber(id);
        return addressDTO;
    }

    @Override
    public AddressDTO edit(Integer id, AddressDTO addressDTO) {
        Optional<Address> optionalAddress = addresses
                .stream()
                .filter(address -> Objects.equals(address.getIdNumber(), id))
                .findFirst();
        Address address = optionalAddress.orElseThrow(() -> new RuntimeException("Address not found with id: " + id));
        address.setAddressLine(address.getAddressLine());
        address.setCity(address.getCity());
        address.setRegion(address.getRegion());
        addressDTO.setIdNumber((Integer) address.getIdNumber());
        return addressDTO;
    }
    @Override
    public String delete(Integer id) {

        try {
            addresses.removeIf(address -> Objects.equals(address.getIdNumber(),id));
            return "O`chirildi";
        } catch (Exception e) {
            e.printStackTrace();
            return "xatolik yuz berdi";
        }
    }

    @Override
    public Address getByIdOrElseThrow(Integer id) {
        Address address = addresses
                .stream()
                .filter(oneAddress -> Objects.equals(oneAddress.getIdNumber(), id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Address not found with id : " + id));
        return address;

    }
}
