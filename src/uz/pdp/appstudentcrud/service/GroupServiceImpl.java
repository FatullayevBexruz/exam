package uz.pdp.appstudentcrud.service;

import uz.pdp.appstudentcrud.entity.Group;
import uz.pdp.appstudentcrud.payload.GroupDTO;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class GroupServiceImpl implements GroupService {

    private static GroupServiceImpl instance;
    public List<Group> groups = Collections.synchronizedList(new ArrayList<>());
    private static Lock lock = new ReentrantLock();
    private GroupServiceImpl(){}
    public static GroupServiceImpl getInstance(){
        if (Objects.isNull(instance)){
            lock.lock();
            if (Objects.isNull(instance))
                instance = new GroupServiceImpl();
            lock.unlock();
        }
        return instance;
    }
    @Override
    public List<GroupDTO> all() {
        return groups
                .stream()
                .map(group -> new GroupDTO((Integer) group.getid(),group.getName()))
                .collect(Collectors.toList());
    }
    @Override
    public GroupDTO add(GroupDTO groupDTO) {
        int id = groups.size() + 1;

        Group genre = new Group(
                id,
                groupDTO.getName()
        );
        groups.add(genre);
        groupDTO.setid(id);

        return groupDTO;
    }
    @Override
    public GroupDTO edit(Integer id, GroupDTO groupDTO) {
        Optional<Group> optionalGroup = groups
                .stream()
                .filter(group -> Objects.equals(group.getid(), id))
                .findFirst();
        Group group = optionalGroup.orElseThrow(() -> new RuntimeException("Group not found with id: " + id));
        group.setName(groupDTO.getName());
        groupDTO.setid((Integer) group.getid());
        return groupDTO;
    }
    @Override
    public String delete(Integer id) {
        try {
            groups.removeIf(group -> Objects.equals(group.getid(),id));
            return "O`chirildi";
        } catch (Exception e) {
            e.printStackTrace();
            return "xatolik yuz berdi";
        }
           }
    @Override
    public Group getByIdOrElseThrow(Integer id) {
        Group group = groups
                .stream()
                .filter(oneGroup -> Objects.equals(oneGroup.getid(), id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Group not found with id : " + id));
        return group;

    }
}
