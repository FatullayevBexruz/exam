package uz.pdp.appstudentcrud.payload;

import java.time.LocalDate;

public class StudentDTO {

    private  Number id;
    private String firstname;
    private String lastname;
    private String phoheNumer;
    private Integer groupid;
    private String addressid;
    private String bioFilePath;
    private LocalDate birthDate;
    private String delete;

    public StudentDTO(Number id, String firstname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoheNumer = phoheNumer;
        this.groupid = groupid;
        this.addressid = addressid;
        this.bioFilePath = bioFilePath;
        this.birthDate = birthDate;
        this.delete = delete;
    }

    public StudentDTO(Number id, String firstname, String lastname, String phoheNumer, LocalDate birthDate, String bioFilePath, Number getid, Number idNumber) {
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhoheNumer() {
        return phoheNumer;
    }

    public void setPhoheNumer(String phoheNumer) {
        this.phoheNumer = phoheNumer;
    }

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public String getAddressid() {
        return addressid;
    }

    public void setAddressid(String addressid) {
        this.addressid = addressid;
    }

    public String getBioFilePath() {
        return bioFilePath;
    }

    public void setBioFilePath(String bioFilePath) {
        this.bioFilePath = bioFilePath;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }

    @Override
    public String toString() {
        return "StudentDTO{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phoheNumer='" + phoheNumer + '\'' +
                ", groupid=" + groupid +
                ", addressid='" + addressid + '\'' +
                ", bioFilePath='" + bioFilePath + '\'' +
                ", birthDate=" + birthDate +
                ", delete='" + delete + '\'' +
                '}';
    }
}
