package uz.pdp.appstudentcrud.payload;

public class GroupDTO {
    private Number id;

    private String name;

    public GroupDTO(Number numbergroup, String name) {
        id = numbergroup;
        this.name = name;
    }

    public Number getid() {
        return id;
    }

    public void setid(Number numbergroup) {
        id = numbergroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
