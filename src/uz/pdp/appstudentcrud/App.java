package uz.pdp.appstudentcrud;

import uz.pdp.appstudentcrud.payload.GroupDTO;
import uz.pdp.appstudentcrud.payload.StudentDTO;
import uz.pdp.appstudentcrud.service.GroupService;
import uz.pdp.appstudentcrud.service.GroupServiceImpl;
import uz.pdp.appstudentcrud.service.StudentService;
import uz.pdp.appstudentcrud.service.StudentServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;



public class App {

    // url --> https://gitlab.com/FatullayevBexruz/exam

    public static void main(String[] args) {

        StudentServiceImpl studentService = StudentServiceImpl.getInstance();
        studentService.deserialize();




//        String read = StudentService.read(studentDTO.getId());
//        System.out.println("read -> " + read);





    }

    private static List<StudentDTO> generateStudent(List<GroupDTO> groupDTOList) {
        StudentService studentService = StudentServiceImpl.getInstance();
        List<StudentDTO> studentDTOList = new ArrayList<>();

        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            int groupIndex = random.nextInt(groupDTOList.size());
            GroupDTO groupDTO = groupDTOList.get(groupIndex);
            StudentDTO studentDTO = studentService.add(new StudentDTO(
                    null,
                    "Bexruz"

            ));
            studentDTOList.add(studentDTO);
        }
        return studentDTOList;
    }

    private static List<GroupDTO> generateGroups() {
        GroupService groupService = GroupServiceImpl.getInstance();

        List<GroupDTO> groupDTOList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            GroupDTO groupDTO = groupService.add(new GroupDTO(null, "Group -> " + i));
            groupDTOList.add(groupDTO);
        }
        return groupDTOList;

    }

}
