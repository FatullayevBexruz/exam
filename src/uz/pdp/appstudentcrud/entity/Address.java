package uz.pdp.appstudentcrud.entity;

public class Address {
    private Number idNumber;
    private String region;
    private String city;
    private String addressLine;

    public Address(Number idNumber, String region, String city, String addressLine) {
        this.idNumber = idNumber;
        this.region = region;
        this.city = city;
        this.addressLine = addressLine;
    }

    public Number getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Number idNumber) {
        this.idNumber = idNumber;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    @Override
    public String toString() {
        return "Address{" +
                "idNumber=" + idNumber +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", addressLine='" + addressLine + '\'' +
                '}';
    }
}
