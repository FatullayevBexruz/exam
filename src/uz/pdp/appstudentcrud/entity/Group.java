package uz.pdp.appstudentcrud.entity;

public class Group {

    private Number id;

   private String name;

    public Group(Integer numbergroup, String name) {
        id = numbergroup;
        this.name = name;
    }

    public Number getid() {
        return id;
    }

    public void setid(Integer numbergroup) {
        id = numbergroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
