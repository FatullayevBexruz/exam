package uz.pdp.appstudentcrud.entity;

import java.time.LocalDate;

public class Student {

    private Number id;
    private String firstname;
    private String lastname;
    private String phoheNumer;
    private Group group;
    private Address address;
    private String bioFilePath;
    private LocalDate birthDate;
    private String delete;

    public Student(Number id, String firstname, String lastname, String phoheNumer, Group group, Address address, String bioFilePath,String delete,LocalDate birthDate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoheNumer = phoheNumer;
        this.group = group;
        this.address = address;
        this.bioFilePath = bioFilePath;
        this.delete=delete;
        this.birthDate=birthDate;
    }

    public Student(int id, String firstname, String lastname, String phoheNumer, String bioFilePath, LocalDate birthDate, Group group) {
    }

    public Number getId() {
        return id;

    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhoheNumer() {
        return phoheNumer;
    }

    public void setPhoheNumer(String phoheNumer) {
        this.phoheNumer = phoheNumer;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getBioFilePath() {
        return bioFilePath;
    }

    public void setBioFilePath(String bioFilePath) {
        this.bioFilePath = bioFilePath;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phoheNumer='" + phoheNumer + '\'' +
                ", group=" + group +
                ", address=" + address +
                ", bioFilePath='" + bioFilePath + '\'' +
                ", birthDate=" + birthDate +
                ", delete='" + delete + '\'' +
                '}';
    }
}
